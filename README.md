## Website Logger
A thing I made in learning Rust.

It currently works and I can't find any remaining issues, the main problems being things like error handling. I am sure the code is not written *well* but it does work.

This is a CLI, the commands consist of:
```
Commands:
  get     Get the website and check for updates
  delete  Delete stored website logs
  help    Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help
  -V, --version  Print version
```