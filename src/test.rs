use super::*;

#[test]
fn check_hash() {
    assert_eq!(
        scraper::hash::hash(&"hello".to_string()),
        "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824"
    )
}

#[test]
#[ignore]
fn check_get_page_content() {
    assert_eq!(
        scraper::scraper::get_page_content(&"example.com"),
        "Example Domain 
     This domain is for use in illustrative examples in documents. You may use this
    domain in literature without prior coordination or asking for permission. 
     More information..."
    );
}

#[test]
fn check_count_chars() {
    assert_eq!(
        scraper::scraper::count_chars(
            &"la;j jenad sjjasdj! 123 nakjs a...// アイウエオ".to_string(),
            'a'
        ),
        5
    );
}
