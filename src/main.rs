mod scraper;

use clap::{Parser, Subcommand};
use colored::Colorize;

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    /// Get the website and check for updates
    Get {
        #[arg(long)]
        url: String,
        #[arg(long, default_value_t = 'a')]
        char: char,
    },

    /// Delete stored website logs
    Delete {
      #[arg(short, long, default_value_t = true)]
      confirm: bool,
    }
}

fn main() {
    let cli = Cli::parse();

    match &cli.command {
        Some(Commands::Get { url, char }) => {
            scraper::scraper::check_for_missing_files();
            println!("{}", "Getting URL...".italic());
            let content = scraper::scraper::get_page_content(&url);
            let hash = scraper::hash::hash(&content);

            let change_status = scraper::hash::compare_hashes(&hash);
            let number_chars = scraper::scraper::count_chars(&content, *char);
            scraper::scraper::log_content(&content);
            scraper::hash::save_current_hash(&hash);

            scraper::scraper::ferris_say(&change_status);

            println!(
                "{} {}'s were found!",
                number_chars.to_string().blue(),
                char.to_uppercase()
            );
        }

        Some(Commands::Delete { confirm }) => {
          match confirm {
            true => scraper::scraper::delete_all(),
            _ => return,
          }
        }

        None => {}
    }
}

#[cfg(test)]
mod test;