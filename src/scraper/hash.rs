use sha2::{Digest, Sha256};
use std::fs::File;
use std::io::prelude::*;

pub fn hash(input: &String) -> String {
    let mut hasher = Sha256::new();
    hasher.update(input.as_bytes());
    let result = hasher.finalize();
    let result = format!("{:x}", result);
    return result;
}

pub fn compare_hashes(new_hash: &String) -> &str {
    let mut file = File::open("src/logs/current_hash.txt").unwrap();
    let mut current_hash = String::new();
    file.read_to_string(&mut current_hash).unwrap();

    if new_hash == &current_hash {
        return "The website is the same";
    } else if current_hash == "" {
        return "After the first run, you can see if the website has changed";
    } else if new_hash != &current_hash {
        return "The website has changed";
    } else {
        return "Unable to determine if the website has changed";
    }
}

pub fn save_current_hash(hash: &String) {
    File::create("src/logs/current_hash.txt").unwrap();
    let mut file = File::create("src/logs/current_hash.txt").unwrap();
    file.write_all(hash.as_bytes()).unwrap();
}
