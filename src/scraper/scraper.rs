use webpage::{Webpage, WebpageOptions};

use chrono::Utc;

use std::fs;
use std::fs::File;
use std::io::prelude::*;

use std::path::PathBuf;

use ferris_says::say;
use std::io::{stdout, BufWriter};

use colored::Colorize;

pub fn get_page_content(url: &str) -> String {
    let info = Webpage::from_url(url, WebpageOptions::default())
        .expect("Could not read load URL, does it start with 'https'");
    let html = info.html;
    println!("{}. Saving...", "Page content retrieved".bold().on_green());
    return html.text_content.trim().to_string();
}

pub fn log_content(content: &String) {
    fs::create_dir_all("src/logs/archive").unwrap();
    let time = format!("src/logs/archive/{}.txt", Utc::now());
    let mut file = File::create(time.to_string()).unwrap();

    file.write_all(content.as_bytes()).unwrap();
}

pub fn count_chars(content: &String, character: char) -> u64 {
    let content = String::from(content);
    let content_slice = &content[..];

    let mut number_chars: u64 = 0;

    for i in content_slice.chars() {
        if i == character {
            number_chars = number_chars + 1;
        } else {
            continue;
        }
    }

    number_chars
}

pub fn ferris_say(text: &str) {
    let width = 24;

    let mut writer = BufWriter::new(stdout());
    say(text, width, &mut writer).unwrap();
}

pub fn delete_all() -> () {
  fs::remove_dir_all("src/logs/").unwrap();
}

pub fn check_for_missing_files() {
    fs::create_dir_all("src/logs/").unwrap();
    let path = PathBuf::from("src/logs/current_hash.txt");
  
    if path.is_file() {
      return;
    } else {
       File::create("src/logs/current_hash.txt").unwrap();      
    }
}